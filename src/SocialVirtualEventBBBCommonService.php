<?php

namespace Drupal\social_virtual_event_bbb;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use Drupal\social_virtual_event_bbb\Entity\VirtualEventBBBConfigEntity;
use Drupal\social_virtual_event_bbb\Entity\VirtualEventBBBConfigEntityInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Defines Social Virtual Event BBB Common Service.
 */
class SocialVirtualEventBBBCommonService {

  use StringTranslationTrait;
  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * Configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SocialVirtualEventBBBCommonService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entityTypeManager,
    TranslationInterface $string_translation
    ) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Get all allowed recording access options.
   *
   * @return array
   *   Array of allowed recording access options.
   */
  public function getAllAllowedRecordingAccessOptions() {
    return [
      'recording_access_viewer' => $this->t('All visitors (including public)'),
      'recording_access_viewer_authenticated' => $this->t('All authenticated users'),
      'recording_access_viewer_moderator' => $this->t('All Organisers'),
      'recording_access_viewer_enrolled' => $this->t('All Organisers, Group members & enrolled'),
      'recording_access_viewer_group' => $this->t('All Organisers & Group members'),
    ];
  }

  /**
   * Get all allowed download access options.
   *
   * @return array
   *   Array of allowed download access options.
   */
  public function getAllAllowedDownloadAccessOptions() {
    return [
      'download_access_viewer' => $this->t('All visitors (including public)'),
      'download_access_viewer_authenticated' => $this->t('All authenticated users'),
      'download_access_viewer_moderator' => $this->t('All Organisers'),
      'download_access_viewer_enrolled' => $this->t('All Organisers, Group members & enrolled'),
      'download_access_viewer_group' => $this->t('All Organisers & Group members'),
    ];
  }

  /**
   * Get the font size from settings.
   *
   * @return
   *   font size as string or FALSE.
   */
  public function getFontSize() {
    $config = $this->configFactory->getEditable('social_virtual_event_bbb.settings');
    $font_size = $config->get('count_down_font_size');
    if (isset($font_size)) {
      return $config->get('count_down_font_size');
    }
    return FALSE;
  }

  /**
   * Nodejs Callback.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The given node object.
   *
   * @return array
   *   Meeting info.
   */
  public function nodejsGetBBBStatistic(NodeInterface $node) {

    $meeting_info = FALSE;
    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $entity_type = $node->getEntityTypeId();
    $entity_id = $node->id();
    $event = $virtualEventsCommon->getEventByReference($entity_type, $entity_id);

    if ($event) {
      $event_config = $event->getVirtualEventsConfig();
      $source_data = $event->getSourceData('virtual_event_bbb');
      $source_config = $event_config->getSourceConfig('virtual_event_bbb');
      if (!isset($source_config) && !$source_config['data']['key_type']) {
        return;
      }

      $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
      $keys = $keyPlugin->getKeys($source_config);
      $apiUrl = $keys["url"];
      $secretKey = $keys["secretKey"];
      $bbb = new VirtualEventBBB($secretKey, $apiUrl);
      $getMeetingInfoParameters = new GetMeetingInfoParameters($event->id(), $source_data['settings']['moderatorPW']);

      try {
        $response = $bbb->getMeetingInfo($getMeetingInfoParameters);
        if ($response->getReturnCode() === 'SUCCESS') {
          if (!empty($response->getRawXml())) {
            $meeting_name = $response->getRawXml()->meetingName->__toString();
            $meeting_viewers = $response->getRawXml()->participantCount->__toString();
            $meeting_moderators = $response->getRawXml()->moderatorCount->__toString();
            $meeting_running = $response->getRawXml()->running->__toString();
            $meeting_joined = $response->getRawXml()->hasUserJoined->__toString();

            $meeting_info = [
              'meeting_name' => $meeting_name,
              'count_viewers' => $meeting_viewers,
              'count_moderators' => $meeting_moderators,
              'meeting_running' => $meeting_running,
              'meeting_joined' => $meeting_joined,
            ];
          }
        }

      }
      catch (\RuntimeException $exception) {
        $this->getLogger('social_virtual_event_bbb')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't get meeting info! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
    }

    return $meeting_info;

  }

  /**
   * Get Virtual event BBB Entity config by Node Id.
   *
   * @param int $nid
   *   The node id.
   *
   * @return
   *   \Drupal\social_virtual_event_bbb\Entity\VirtualEventBBBConfigEntityInterface or FALSE
   */
  public function getSocialVirtualEventBBBEntityConfig($nid) {

    if (isset($nid) && is_numeric($nid)) {
      $virtual_event_bbb_config_entity = $this->entityTypeManager
        ->getStorage('virtual_event_bbb_config_entity')
        ->load($nid);

      if ($virtual_event_bbb_config_entity instanceof VirtualEventBBBConfigEntityInterface) {
        return $virtual_event_bbb_config_entity;
      }
    }

    return FALSE;

  }

  /**
   * Create new virtual event bbb Entity Config.
   *
   * @param int $nid
   *   The node id.
   * @param array $config_data
   *   The config data.
   */
  public function createSocialVirtualEventBBBEntityConfig($nid, $config_data) {

    $config = VirtualEventBBBConfigEntity::create([
      'id' => $nid,
    ]);
    $config->setRecordingAccess($config_data['recording_access']);
    $config->setJoinButtonVisibleBefore($config_data['join_button_visible_before']);
    $config->setJoinButtonVisibleAfter($config_data['join_button_visible_after']);
    $config->setNode($nid);
    $config->save();

  }

  /**
   * Update virtual event BBB Entity config.
   *
   * @param int $nid
   *   The node id.
   * @param array $config_data
   *   The config data.
   */
  public function updateSocialVirtualEventBBBEntityConfig($nid, $config_data) {

    $virtual_event_bbb_config_entity = $this->entityTypeManager
      ->getStorage('virtual_event_bbb_config_entity')
      ->load($nid);

    if ($virtual_event_bbb_config_entity instanceof VirtualEventBBBConfigEntityInterface) {
      $virtual_event_bbb_config_entity->setRecordingAccess($config_data['recording_access']);
      $virtual_event_bbb_config_entity->setJoinButtonVisibleBefore($config_data['join_button_visible_before']);
      $virtual_event_bbb_config_entity->setJoinButtonVisibleAfter($config_data['join_button_visible_after']);
      $virtual_event_bbb_config_entity->save();
    }

  }

  /**
   * Get options for join meeting button.
   */
  public function getOptionsForJoinMeetingButton() {
    return [
      'show_always_open' => $this->t('Show always open'),
      '5' => $this->t('5 minutes'),
      '10' => $this->t('10 minutes'),
      '15' => $this->t('15 minutes'),
      '30' => $this->t('30 minutes'),
      '45' => $this->t('45 minutes'),
      '60' => $this->t('60 minutes'),
      '90' => $this->t('90 minutes'),
    ];
  }

  /**
   * Nodejs support active or not.
   */
  public function isNodejsActive() {
    $config = $this->configFactory->getEditable('social_virtual_event_bbb.settings');
    $nodejs = $config->get('nodejs_support') ? $config->get('nodejs_support') : FALSE;
    if ($nodejs) {
      return TRUE;
    }

    return FALSE;

  }

  /**
   * Create Meeting Callback for BBB.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return bool | url
   *   FALSE or the url object for the callback link
   */
  public function createMeetingCallback(NodeInterface $node) {

    $callbackLink = FALSE;

    $config = $this->configFactory->getEditable('social_virtual_event_bbb.settings');

    // Don't create callback when setting is FALSE.
    if ($config->get('add_bbb_server_callback') === FALSE) {
      return $callbackLink;
    }

    // Construct callback Url based on given node.
    $callbackHost = \Drupal::request()->getSchemeAndHttpHost();
    $callbackPath = '/api/bbb-meeting-webhook/' . $node->id();
    $callbackUrl = Url::fromUri($callbackHost . $callbackPath);
    $callbackUrl->setOption('query', [
      '_format' => 'json',
    ]);
    $callbackLink = $callbackUrl->toString();

    return $callbackLink;

  }
  
  public function getBBBLogo() {
  
    $file_url = NULL;
  
    $bbb_logo = $this->configFactory->getEditable('social_virtual_event_bbb.settings')->get('bbb_logo');
    if (isset($bbb_logo)) {
      if ($file = $this->entityTypeManager->getStorage('file')->load($bbb_logo)) {
        $file_url = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
      }     
    }   
    
    return $file_url;
    
  }  
  


}
