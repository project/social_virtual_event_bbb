<?php

namespace Drupal\social_virtual_event_bbb\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\social_group\SocialGroupHelperService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\virtual_event_bbb\Form\VirtualEventBBBLinkForm;

/**
 * Provides a 'SocialVirtualEventBBBJoinButtonBlock' block.
 *
 * @Block(
 *  id = "social_virtual_event_bbb_join_button_block",
 *  admin_label = @Translation("Social Virtual Event BBB Join Button Block"),
 * )
 */
class SocialVirtualEventBBBJoinButtonBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Group helper service.
   *
   * @var \Drupal\social_group\SocialGroupHelperService
   */
  protected $groupHelperService;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Social Virtual Event BBB Statistic Block constructor.
   *
   * @param array $configuration
   *   The given configuration.
   * @param string $plugin_id
   *   The given plugin id.
   * @param mixed $plugin_definition
   *   The given plugin definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\social_group\SocialGroupHelperService $groupHelperService
   *   The group helper service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.   *.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $routeMatch, SocialGroupHelperService $groupHelperService, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->groupHelperService = $groupHelperService;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('social_group.helper_service'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {

    $node = \Drupal::routeMatch()->getParameter('node');

    // Checking for a node object.
    if (!is_object($node) && !is_null($node)) {
      $node = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->load($node);
    }
    // Check for an node type of event.
    if ($node instanceof NodeInterface && $node->getType() === 'event') {
      return AccessResult::allowed();
    }

    return AccessResult::neutral();

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $cache_contexts = parent::getCacheContexts();
    return $cache_contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $cache_tags = parent::getCacheTags();
    return $cache_tags;
  }

  /**
   *
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $buttons = [];

    // Get current node so we can build correct links.
    $node = \Drupal::routeMatch()->getParameter('node');

    // Checking for a node object.
    if (!is_object($node) && !is_null($node)) {
      $node = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->load($node);
    }
    // Check for an node type of event.
    if ($node instanceof NodeInterface && $node->getType() === 'event') {
      $virtualEventsCommon = \Drupal::service('virtual_events.common');
      $entity_type = $node->getEntityTypeId();
      $entity_id = $node->id();
      $event = $virtualEventsCommon->getEventByReference($entity_type, $entity_id);
      if ($event) {
        // Add pseude display settings
        // to avoid errors
	      $display_options = [];
        $display_options["join_button_text"] = '';
        // Get join meeting form including $display_options
        $joinmeeting_button = \Drupal::formBuilder()->getForm(VirtualEventBBBLinkForm::class, $event, $display_options);
        if (isset($joinmeeting_button) && 
	    isset($joinmeeting_button['submit']) && 
	    $joinmeeting_button['submit']['#access'] === TRUE) {
          $build['#prefix'] = '<div class="card__block text-center">';
          $build['#suffix'] = '</div>';
          $build['content'] = $joinmeeting_button;
        }
      }
    }

    return $build;

  }

}
