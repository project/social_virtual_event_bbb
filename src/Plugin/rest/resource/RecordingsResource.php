<?php

namespace Drupal\social_virtual_event_bbb\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\virtual_event_bbb\Plugin\VirtualEvent\FormatterPlugin\VirtualEventBBBFormatter;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\virtual_events\Entity\VirtualEventsFormatterEntity;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;

/**
 * Represents Recordings records as resources.
 *
 * @RestResource (
 *   id = "social_virtual_event_bbb_recordings",
 *   label = @Translation("Recordings"),
 *   uri_paths = {
 *     "canonical" = "/api/social-virtual-event-bbb-recordings/{node_id}",
 *     "create" = "/api/social-virtual-event-bbb-recordings-delete"
 *   }
 * )
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
class RecordingsResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;  

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $current_user, $entity_type_manager);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param int $node_id
   *   The node ID of the open social event.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The response containing the record.
   */
  public function get($node_id) {

    // Throw exception when there is no valid paramter.
    if (!isset($node_id) || empty($node_id)) {
      throw new BadRequestHttpException('The client did not give the node id for the event!');
    }
    // Throw exception when no node can be found.
    $node = $this->entityTypeManager->getStorage('node')->load($node_id);
    if (!$node instanceof NodeInterface) {
      throw new UnprocessableEntityHttpException('There is no event with that id!');
    }

    // Per default 

    $grant_access = FALSE;

    $user = $this->currentUser;
    $entity_type = $node->getEntityTypeId();
    $entity_bundle = $node->bundle();
    $entity_id = $node->id();
    $entity_uuid = $node->uuid();
    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $socialVirtualEventsCommon = \Drupal::service('social_virtual_event_bbb.common');
    $access_delete_recording = FALSE;
    $event_id = 'event_' . $entity_uuid;  

    try {

      // Load The event.
      $event = $virtualEventsCommon->getEventById($event_id);

      if (!$event) {
        throw new UnprocessableEntityHttpException('There is no virtual event for recordings!');
      }

      $entity = $event->getEntity();
      $enabled_event_source = $event->getEnabledSourceKey();      
      $event_config = $event->getVirtualEventsConfig($enabled_event_source);
      $source_config = $event_config->getSourceConfig($enabled_event_source);     
      $source_data = $event->getSourceData();
      $eventSourcePlugin = $event->getEventSourcePlugin();
      if(!isset($source_config["data"]["key_type"])) {
        throw new UnprocessableEntityHttpException('There is no valid api key!');    
      }        
        
      // Check if we have to restrict recording access.
      $virtual_event_bbb_config_entity = $socialVirtualEventsCommon->getSocialVirtualEventBBBEntityConfig($entity_id); 
      
      \Drupal::logger('debug')->debug('<pre><code>' . print_r($virtual_event_bbb_config_entity, TRUE) . '</code></pre>');   
        
      if ($virtual_event_bbb_config_entity) {        

        $grant_access = FALSE;
        $recording_access = $virtual_event_bbb_config_entity->getRecordingAccess();

        if ($recording_access === 'recording_access_viewer_moderator') {
          if ($entity->access('update')) {
            $grant_access = TRUE;
          }
        }     

        if ($recording_access === 'recording_access_viewer') {
          if ($entity->access('view')) {
            $grant_access = TRUE;
          }
        }

        if ($recording_access === 'recording_access_viewer_authenticated') {
          if ($entity->access('view') && $user->isAuthenticated()) {
            $grant_access = TRUE;
          }
        }

        if ($recording_access === 'recording_access_viewer_enrolled') {
          // Set member to false.
          $is_member = FALSE;
          $event_enrollment = \Drupal::entityTypeManager()->getStorage('event_enrollment');
          $enrolled = $event_enrollment->loadByProperties([
            'field_account' => $user->id(),
            'field_event' => $entity_id,
            'field_enrollment_status' => 1,
          ]);

          $group = _social_group_get_current_group($entity);
          
          // Get Account.
          $account = \Drupal::entityTypeManager()
            ->getStorage('user')
            ->load($user->id());

          if ($group instanceof GroupInterface) {
            $is_member = $group->getMember($account);
          }

          if ($entity->access('view') && ($enrolled || $is_member)) {
            $grant_access = TRUE;
          }
        }

        if ($recording_access === 'recording_access_viewer_group') {
          // Set member to false.
          $is_member = FALSE;

          $group = _social_group_get_current_group($entity);

          // Get Account.
          $account = \Drupal::entityTypeManager()
            ->getStorage('user')
            ->load($user->id());

          if ($group instanceof GroupInterface) {
            $is_member = $group->getMember($account);
          }

          if ($entity->access('update') || $is_member) {
            $grant_access = TRUE;
          }
        }                
      }
      else {
        throw new UnprocessableEntityHttpException('There is no configuration entity to control recording access!');  
      }

      // If we do not have any right to access
      if (!$grant_access) {
        throw new UnprocessableEntityHttpException('The current user has no right to access this event!');
      }

      // Check if a user may delete a recording.
      if ($user->hasPermission('delete virtual event recordings')) {
        $access_delete_recording = TRUE;
      }

      $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
      $keys = $keyPlugin->getKeys($source_config);

      $apiUrl = $keys["url"];
      $secretKey = $keys["secretKey"];
      $bbb = new VirtualEventBBB($secretKey, $apiUrl);

      $recordingParams = new GetRecordingsParameters();
      $recordingParams->setMeetingID($event->id());

      try {
        $bbb_response = $bbb->getRecordings($recordingParams);
        $bbb_recordings = $bbb_response->getRawXml()->recordings->recording;
        if (empty($bbb_recordings)) {
          throw new UnprocessableEntityHttpException('There is no recording result for that event!');
        }
        else {
          $i = 1;
          $recordings = [];
          foreach ($bbb_recordings as $key => $recording) {
            foreach ($recording->playback as $key => $playback) {
              foreach ($recording->playback->format as $key => $format) {
                if (
                  $format->type == "video_mp4" || 
                  $format->type == "video" || 
                  $format->type == "presentation" ||
                  $format->type == "screenshare"
                ) {
                  $format->recordID = $recording->recordID;
                  $recording_id = $recording->recordID->__toString();
                  $recording_url = $format->url;
                  $recording_date = _social_virtual_event_bbb_get_recording_date($recording_id);
                  $delete_form = FALSE;                  
                  if ($access_delete_recording) {
                    $delete_form = \Drupal::formBuilder()->getForm('\Drupal\social_virtual_event_bbb\Form\SocialVirtualEventBBBDeleteRecordingForm', $recording_id, $entity_type, $entity_id);
                  }

                  $recordings[] = [
                    'weight' => $i,
                    'recording_url' => $format->url->__toString(),
                    'recording_format' => $format->type->__toString(),
                    'record_id' => $recording_id,
                    'recording_date' => $recording_date,
                    'delete_form' => $delete_form,
                  ];
                  $i = $i + 1;
                }
              }
            }
          }
        }

        if(isset($recordings) && !empty($recordings)) {
          $response = new ModifiedResourceResponse($recordings, 201);
        }
        else {
          throw new UnprocessableEntityHttpException('There is no recording result for that event!'); 
        }
      }
      catch(UnprocessableEntityHttpException $exception) {
        $this->logger->warning($exception->getMessage());            
        $error['error'] = $exception->getMessage();
        $error['info'] = 'error';
        $response = new ModifiedResourceResponse($error, 422);    
      }
      catch (\Exception $exception) {
        $this->logger->warning($exception->getMessage());            
        $error['error'] = $exception->getMessage();
        $error['info'] = 'error';
        $response = new ModifiedResourceResponse($error, 400);
      }
    }
    catch(UnprocessableEntityHttpException $exception) {
      $this->logger->warning($exception->getMessage());            
      $error['error'] = $exception->getMessage();
      $error['info'] = 'error';
      $response = new ModifiedResourceResponse($error, 422);    
    }
    catch (\Exception $exception) {
      $this->logger->warning($exception->getMessage());
      $error['error'] = $exception->getMessage();
      $error['info'] = 'error';
      $response = new ModifiedResourceResponse($error, 400);
    }

    return $response;
  
  }

  /**
   * Responds to DELETE requests.
   *
   * @param int $recording_id
   *   The ID of the recording.
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity ID of the event. 
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(array $data = ['error' => 'error']) {

    $user = $this->currentUser;
    


    try {

      // We need to check if data is present and error.
      if (isset($data['error'])) {
        $this->logger->warning("Data can't be empty.");
        throw new BadRequestHttpException("Data can't be empty.");
      }

      if (
        !isset($data['recording_id']) || empty($data['recording_id']) &&
        !isset($data['entity_type']) || empty($data['entity_type']) &&
        !isset($data['entity_id']) || empty($data['entity_id'])
      ) {
        throw new BadRequestHttpException('The client does not provide valid paramteters inside the request body!');  
      }

      $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
      $virtualEventsCommon = \Drupal::service('virtual_events.common');
      $event = $virtualEventsCommon->getEventByReference($data['entity_type'], $data['entity_id']);

      if (!$event) {
        throw new UnprocessableEntityHttpException('There is no event to delete a recording from!');  
      }
      
      $event_config = $event->getVirtualEventsConfig();
      $source_data = $event->getSourceData('virtual_event_bbb');
      $source_config = $event_config->getSourceConfig('virtual_event_bbb');
        
      if (!isset($source_config) && !$source_config['data']['key_type']) {
        throw new UnprocessableEntityHttpException('No valid API Keys or problem fetching them!');  
      }

      // Check if a user may delete a recording.
      if ($user->hasPermission('delete virtual event recordings')) {
        $access_delete_recording = TRUE;
      }
      else {
        throw new UnprocessableEntityHttpException('This account is not allowed to delete a recoding!');   
      }

      $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
      $keys = $keyPlugin->getKeys($source_config);
      $apiUrl = $keys["url"];
      $secretKey = $keys["secretKey"];
      $bbb = new VirtualEventBBB($secretKey, $apiUrl);
      $deleteRecordingsParameters = new DeleteRecordingsParameters($data['recording_id'], $source_data['settings']['moderatorPW']);

      try {
        $bbb_response = $bbb->deleteRecordings($deleteRecordingsParameters);
        $recording_deleted = $bbb_response->getRawXml()->returncode->__toString();
        if ($recording_deleted === 'FAILED') {
          throw new UnprocessableEntityHttpException('No recording found. Nothing was deleted!');
        }
        else {
          $bbb_data = [
            'recording_id' => $data['recording_id'],
            'status' => 'recording deleted'
          ];
          $response = new ModifiedResourceResponse($bbb_data, 201);
        }
      }
      catch(UnprocessableEntityHttpException $exception) {
        $this->logger->warning($exception->getMessage());            
        $error['error'] = $exception->getMessage();
        $error['info'] = 'error';
        $response = new ModifiedResourceResponse($error, 422);    
      }
      catch (\Exception $exception) {
        $this->logger->warning($exception->getMessage());
        $error['error'] = $exception->getMessage();
        $error['info'] = 'error';
        $response = new ModifiedResourceResponse($error, 400);
      }
    }
    catch(UnprocessableEntityHttpException $exception) {
      $this->logger->warning($exception->getMessage());            
      $error['error'] = $exception->getMessage();
      $error['info'] = 'error';
      $response = new ModifiedResourceResponse($error, 422);    
    }
    catch (\Exception $exception) {
      $this->logger->warning($exception->getMessage());
      $error['error'] = $exception->getMessage();
      $error['info'] = 'error';
      $response = new ModifiedResourceResponse($error, 400);
    }

    return $response;

  }
}
