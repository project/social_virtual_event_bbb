<?php

namespace Drupal\social_virtual_event_bbb\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\virtual_event_bbb\Plugin\VirtualEvent\FormatterPlugin\VirtualEventBBBFormatter;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\virtual_events\Entity\VirtualEventsFormatterEntity;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;

/**
 * Represents Recordings records as resources.
 *
 * @RestResource (
 *   id = "social_virtual_event_bbb_recording_settings",
 *   label = @Translation("Recording Settings"),
 *   uri_paths = {
 *     "canonical" = "/api/social-virtual-event-bbb-recording-settings/{node_uuid}",
 *     "create" = "/api/social-virtual-event-bbb-recording-settings/{node_uuid}"
 *   }
 * )
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
class RecordingSettingsResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;  

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $current_user, $entity_type_manager);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param int $node_uuid
   *   The node ID of the open social event.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The response containing the record.
   */
  public function get($node_uuid) {

    // Throw exception when there is no valid paramter.
    if (!isset($node_uuid) || empty($node_uuid)) {
      throw new BadRequestHttpException('The client did not give the node uuid for the event!');
    }
    // Throw exception when no node can be found.
    $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties(['uuid' => $node_uuid]);
    if ($node = reset($nodes)) {
      if (!$node instanceof NodeInterface) {
        throw new UnprocessableEntityHttpException('There is no event with that id!');
      }
    }   

    // Load config
    $social_virtual_event_bbb_settings = \Drupal::config('social_virtual_event_bbb.settings');
    // Load service
    $socialVirtualEventsCommon = \Drupal::service('social_virtual_event_bbb.common');
    // Recording Access.
    $default_recording_access_allowed_options = $socialVirtualEventsCommon->getAllAllowedRecordingAccessOptions();
    $recording_access_allowed_options = $social_virtual_event_bbb_settings->get('recording_access_allowed');
    $recording_access_allowed_default_option = $social_virtual_event_bbb_settings->get('recording_access_default');
    // Get Join Meeting Button options.
    $join_meeting_button_options = $socialVirtualEventsCommon->getOptionsForJoinMeetingButton();
    // Join Meeting Button Before.
    $join_meeting_button_before_default_option = $social_virtual_event_bbb_settings->get('join_meeting_button_before_default');
    // Join Meeting Button After.
    $join_meeting_button_after_default_option = $social_virtual_event_bbb_settings->get('join_meeting_button_after_default');
    // Get config for the event.
    $config = $socialVirtualEventsCommon->getSocialVirtualEventBBBEntityConfig($node->id());  
    
    if ($config) {
      $recording_access_default = $config->getRecordingAccess();
      $join_meeting_button_visible_before_default = $config->getJoinButtonVisibleBefore();
      $join_meeting_button_visible_after_default = $config->getJoinButtonVisibleAfter();
    }

    if (!isset($recording_access_allowed_options) || empty($recording_access_allowed_options)) {
      $recording_access_allowed = $default_recording_access_allowed_options;
    }
    else {
      $recording_access_allowed = array_intersect_key($default_recording_access_allowed_options, $recording_access_allowed_options);
    }   
          
      $form['recording_access'] = [
        '#type' => 'select',
        '#title' => t('Who can see the recordings?'),
        '#options' => $recording_access_allowed,
        '#default_value' => $recording_access_default ?? $recording_access_allowed_default_option,
        '#disabled' => $on_translation_disabled,
      ];
          
      $form['join_button_visible_before'] = [
        '#type' => 'select',
        '#title' => t('Display join button before event start'),
        '#default_value' => $join_meeting_button_visible_before_default ?? $join_meeting_button_before_default_option,
        '#options' => $join_meeting_button_options,
        '#disabled' => $on_translation_disabled,
      ];
          
      $form['join_button_visible_after'] = [
        '#type' => 'select',
        '#title' => t('Display join button after event closes'),
        '#default_value' => $join_meeting_button_visible_after_default ?? $join_meeting_button_after_default_option,
        '#options' => $join_meeting_button_options,
        '#disabled' => $on_translation_disabled,
      ];


      try {
        $response = new ModifiedResourceResponse($form, 201);
      }
      catch(UnprocessableEntityHttpException $exception) {
        $this->logger->warning($exception->getMessage());            
        $error['error'] = $exception->getMessage();
        $error['info'] = 'error';
        $response = new ModifiedResourceResponse($error, 422);    
      }
      catch (\Exception $exception) {
        $this->logger->warning($exception->getMessage());            
        $error['error'] = $exception->getMessage();
        $error['info'] = 'error';
        $response = new ModifiedResourceResponse($error, 400);
      }
    

    return $response;
  
  }

  /**
   * Responds to DELETE requests.
   *
   * @param int $recording_id
   *   The ID of the recording.
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity ID of the event. 
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($node_uuid, array $data = ['error' => 'error']) {


    $user = $this->currentUser;  

    try {

      // Throw exception when there is no valid paramter.
      if (!isset($node_uuid) || empty($node_uuid)) {
        throw new BadRequestHttpException('The client did not give the node uuid for the event!');
      }
      // Throw exception when no node can be found.
      $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties(['uuid' => $node_uuid]);
      if ($node = reset($nodes)) {
        if (!$node instanceof NodeInterface) {
          throw new UnprocessableEntityHttpException('There is no event with that id!');
        }
      }
      else {
        throw new UnprocessableEntityHttpException('There is no event with that id!');  
      }  

      if ($node instanceof NodeInterface) {
        $entity_type = $node->getEntityTypeId();
        $entity_bundle = $node->bundle();
        $entity_id = $node->id();

        $social_virtual_event_bbb_settings = \Drupal::config('social_virtual_event_bbb.settings');        
        
          // Set a Drupal state to retrieve the given value later.
          if (isset($data)) {
            $socialVirtualEventsCommon = \Drupal::service('social_virtual_event_bbb.common');
            $isConfig = $socialVirtualEventsCommon->getSocialVirtualEventBBBEntityConfig($node->id());
            if ($isConfig) {
              // Update.
              $update = $socialVirtualEventsCommon->updateSocialVirtualEventBBBEntityConfig($node->id(), $data);
              $update_response = ['recording_access_settings' => 'successfully updated'];
              $response = new ModifiedResourceResponse($update_response, 201);
            }
            else {
              // Create.
              $create = $socialVirtualEventsCommon->createSocialVirtualEventBBBEntityConfig($node->id(), $data);
              $create_response = ['recording_access_settings' => 'successfully created'];
              $response = new ModifiedResourceResponse($create_response, 201);
            }
          }       

      }

      // We need to check if data is present and error.
      if (isset($data['error'])) {
        $this->logger->warning("Data can't be empty.");
        throw new BadRequestHttpException("Data can't be empty.");
      }


    }
    catch(UnprocessableEntityHttpException $exception) {
      $this->logger->warning($exception->getMessage());            
      $error['error'] = $exception->getMessage();
      $error['info'] = 'error';
      $response = new ModifiedResourceResponse($error, 422);    
    }
    catch (\Exception $exception) {
      $this->logger->warning($exception->getMessage());
      $error['error'] = $exception->getMessage();
      $error['info'] = 'error';
      $response = new ModifiedResourceResponse($error, 400);
    }

    return $response;

  }
}
