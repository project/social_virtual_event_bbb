<?php

namespace Drupal\social_virtual_event_bbb\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\DeleteRecordingsParameters;

/**
 *
 */
class SocialVirtualEventBBBDeleteRecordingForm extends FormBase {

  /**
   * Get the recording ID of a recording and sends API Call to delete it.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {

    $form_id = 'social_virtual_event_bbb_delete_recording';

    static $count = 0;
    $count++;

    return $form_id . '_' . $count;

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $recording_id = NULL, $entity_type = NULL, $entity_id = NULL) {

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['recording_id'] = [
      '#type' => 'hidden',
      '#value' => $recording_id,
    ];

    $form['entity_type'] = [
      '#type' => 'hidden',
      '#value' => $entity_type,
    ];

    $form['entity_id'] = [
      '#type' => 'hidden',
      '#value' => $entity_id,
    ];

    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#attributes' => ['onclick' => 'if(!confirm("Are you sure you want to delete that recording?")){return false;}'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $recording_id = $form_state->getValue('recording_id');
    $entity_type = $form_state->getValue('entity_type');
    $entity_id = $form_state->getValue('entity_id');

    if (isset($recording_id) &&
        isset($entity_type) &&
        isset($entity_id)) {

      $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
      $virtualEventsCommon = \Drupal::service('virtual_events.common');

      $event = $virtualEventsCommon->getEventByReference($entity_type, $entity_id);
      if ($event) {
        $event_config = $event->getVirtualEventsConfig();
        $source_data = $event->getSourceData('virtual_event_bbb');
        $source_config = $event_config->getSourceConfig('virtual_event_bbb');
        if (!isset($source_config) && !$source_config['data']['key_type']) {
          return;
        }

        $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
        $keys = $keyPlugin->getKeys($source_config);
        $apiUrl = $keys["url"];
        $secretKey = $keys["secretKey"];
        $bbb = new VirtualEventBBB($secretKey, $apiUrl);
        $deleteRecordingsParameters = new DeleteRecordingsParameters($recording_id, $source_data['settings']['moderatorPW']);

        try {
          $response = $bbb->deleteRecordings($deleteRecordingsParameters);
        }
        catch (\RuntimeException $exception) {
          $this->getLogger('social_virtual_event_bbb')->warning($exception->getMessage());
          $error_message = $this->t("Couldn't get meeting info! please contact system administrator.");
          $this->messenger()->addError($error_message);
        }
      }
    }
  }

}
