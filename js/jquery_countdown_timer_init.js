(function ($, Drupal, drupalSettings) {

  "use strict";

  /**
   * Attaches the JS countdown behavior
   */
  Drupal.behaviors.jsCountdownTimer = {

    attach: function (context) {
      if (drupalSettings.countdown) {

        $.each(drupalSettings.countdown, function (index, value) {
          let ts = new Date(value.unixtimestamp * 1000);
          let ts_event = new Date(value.unixtimestamp_event * 1000);
          let current_date = Math.floor((new Date()) / 1000);
          let form_class = value.form_class + ' button';
          let note_display = $('#' + value.timer_display_element_note);
          let info_display = $('#' + value.timer_display_element_info);
          let event_start_info = Drupal.t('Event open to join!');

          // CountDown Element
          $(context).find('#' + value.timer_display_element).once(value.timer_display_element).countdown({
            timestamp: ts_event,
            font_size: value.fontsize,
            callback: function (days, hours, minutes, seconds) {

              let timer_display_done = days + hours + minutes;

              let dateStrings = new Array();
              dateStrings['@days'] = days + Drupal.t(' day');
              if (days > 1) {
                dateStrings['@days'] = days + Drupal.t(' days');
              }
              dateStrings['@hours'] = hours + Drupal.t(' hour');
              if (hours > 1) {
                dateStrings['@hours'] = hours + Drupal.t(' hours');
              }
              dateStrings['@minutes'] = minutes + Drupal.t(' minute');
              if (minutes > 1) {
                dateStrings['@minutes'] = minutes + Drupal.t(' minutes');
              }
              dateStrings['@seconds'] = seconds + Drupal.t(' second');
              if (seconds > 1) {
                dateStrings['@seconds'] = seconds + Drupal.t(' seconds');
              }

              let message_headline = Drupal.t('Event starts in ');

              if (days == 0) {
                if (hours == 0) {
                  var message = Drupal.t('@minutes.', dateStrings);
                }
                else {
                  var message = Drupal.t('@hours, @minutes.', dateStrings);
                }
              } else {
                var message = Drupal.t('@days, @hours, @minutes.', dateStrings);
              }

              note_display.html('<div>' + message_headline + '</div><div>' + message + '</div>');

              // Hide the Event stars in X when the
              // event has started
              if (timer_display_done == 0) {
                note_display.hide();
              }


            }
          });
          // Timer
          $(context).find('#' + value.timer_element).once(value.timer_element).countdown({
            timestamp: ts,
            font_size: value.fontsize,
            callback: function (days, hours, minutes, seconds) {
              let done = days + hours + minutes + seconds;
              if (done == 0) {
                // show the info 'open to join'
                info_display.html('<div class="text-center">' + event_start_info + '</div>');
                // show the join button
                $('form.' + form_class).removeClass('visually-hidden');
              }
            }
          });
        });
      }
      // Timer Element
      if (drupalSettings.timer) {
        $.each(drupalSettings.timer, function (index, value) {
          let ts = new Date(value.unixtimestamp * 1000);
          let form_class = value.form_class + ' button';
          let info_display = $('#' + value.timer_display_element_info);
          $(context).find('#' + value.event_stopped_element).once(value.event_stopped_element).countdown({
            timestamp: ts,
            font_size: value.fontsize,
            callback: function (days, hours, minutes, seconds) {
              let event_has_stopped = days + hours + minutes + seconds;
              if (event_has_stopped == 0) {
                // Let's destroy our left over timers
                $('#' + value.timer_element).remove();
                $('#' + value.timer_display_element).remove();
                // Remove our 'open to join' info
                info_display.remove();
                // Hide the join button on an entity.
                $('form.' + form_class).addClass('visually-hidden');
                // In case of a block hide it there as well.
                $('.block-social-virtual-event-bbb-join-button-block').addClass('visually-hidden');
              }
            }
          });
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
