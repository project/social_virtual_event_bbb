(function ($, Drupal, drupalSettings) {
  "use strict";
  /**
   * Attaches the JS countdown behavior
   */
  Drupal.behaviors.socialVirtualEventBBBSubmit = {
    attach: function (context) {
      if(drupalSettings.bbb_submit) {
        $.each( drupalSettings.bbb_submit, function( index, value ) {               
          let button = '.' + value.form_class + ' button';
          let form_class = '.' +  value.form_class;
          $(context).find(button).click(function(){
            let label = Drupal.t('Please reload the page to re-join!');
            let dataId = $(form_class).data('drupal-form-submit-last');
            if (dataId) { 
              $(this).text(label);
            }
          });
        });              
      }
    }
  };
})(jQuery, Drupal, drupalSettings);








