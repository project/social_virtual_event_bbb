# social_virtual_event_bbb
This module overwrites the handling for Virtual Event BBB Meetings

## Installation
Install this module like any other Drupal module.

## Configuaration
After installing the module please configure as followed:
- 1. Visit your content type 'event' admin/structure/types/manage/event/display and enable the new view mode 'BBB Recording'
- 2. Now make sure that the display 'BBB Recording' shows only the field formatter 'Virtual Event BBB Meeting Formatter' and the 'Get the related GROUP NAME group groups for this entity. Depending on your available groups. You can watch at a different view mode to see what you need.
- 3. Visit your Block Layout and enable the following blocks: 'BBB Recording List', 'Social Virtual Event BBB Join Button Block' and if you have activated NODEJS you may want to enable 'Social Virtual Event BBB Statistics Block'
- 4. Please make sure that you enalbe block visibility. All blocks work only on Node type events.
- 5. Visit admin/virtual_events/settings and save your preferred settings for the module.
- 6. Visit admin/virtual_events/virtual_events_formatter_entity/event_display_settings. Please tick [x] Show only recordings and [x] Show in entity page if you want to show the recordings only and show the "join meeting button" as block only.

## Nodejs - Installation
You need to have Node.js version 10+ installed on your system. 
- 1. https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-18-04-de
- 2. npm install drupal-node.js
- 3. Be sure the install the app outside of Drupal's root directory. The correct installation root would be the main direcotry of your project where your composer.json lives. After installing you should see node_modules inside your projects main directory.
- 4. Visit the backend config for nodejs: admin/config/nodejs/settings
- 5. Define Host name for NODEJS Server, The server port should be set to 8080, if available. Define a service key and rember that key. Define Node.js server host for client javascript. Set port again to 8080 if possible.
- 6. Now visit the nodejs config file inside node_modules/drupal-node.js/nodejs.config.js

Exmaple:

```
settings = {
  scheme: 'http',
  port: 8080,
  host: 'localhost',
  serviceKey: 'YOUR_SERVICE_KEY',
  backend: {
    port: 80,
    host: 'yourwebiste.com',
    scheme: 'http',
    basePath: '',
    messagePath: '/nodejs/message'
  },
  debug: true,
  bodyParserJsonLimit: '1mb',
  sslKeyPath: '',
  sslCertPath: '',
  sslCAPath: '',
  baseAuthPath: '/nodejs/',
  extensions: [],
  socketOptions: {}
};

```

- 7. Now start the app with node app.js inside that directory. On production you may want to use forever or pm2 to make sure nodejs will be running without the need to start it all the time.


## Dependencies
Make sure you have installed and enabled the following modules:
- virtual_event_bbb
- social_event_an_enroll

## How that module works
This module acts on event content types only and uses the enrollment handling from open social to show or hide the "Join Meeting" link. It uses the open social enrollment service for "anonymous users" and grants access to virtual events once a valid token has been detected. Please note that this works only for "Public groups" as designed by OpenSocial.

Please note: The new block should replace the block view, because when setting "Show in entity page" has been disabled, the view does not show any results. So now the 'join meeting button' is a custom block and does not use any view or view mode.

## Rest Endpoint to retrieve recordings

- GET Request: api/social-virtual-event-bbb-recordings/{node_id}?_format=json

Parameters:

node_id: This is the integer id of the event node.

Example response for an error:

{
    "error": "The current user has no right to access this event!",
    "info": "error"
}

Successful response:

[
    {
        "weight": 1,
        "recording_url": "https://bigbluebutton-server.example.com/playback/presentation/2.3/058c345a6433b7e9d30f87b4f5ab762ecb3bde08-1714927029305",
        "recording_format": "presentation",
        "record_id": "058c345a6433b7e9d30f87b4f5ab762ecb3bde08-1714927029305",
        "recording_date": "05.05.2024 - 18:37",
        "delete_form": false
    }
]

Note: The "delete_form" is only available if the user has the permission "delete virtual event recordings"!

- POST Request: api/social-virtual-event-bbb-recordings-delete

Request Body:

{
  "recording_id": "058c345a6433b7e9d30f87b4f5ab762ecb3bde08-1714927029305",
  "entity_type": "node",
  "entity_id": 31
}

Example responses for an error:

{
    "error": "There is no event to delete a recording from!",
    "info": "error"
}

{
    "error": "No recording found. Nothing was deleted!",
    "info": "error"
}

Successful response:

{
    "recording_id": "058c345a6433b7e9d30f87b4f5ab762ecb3bde08-1714927029305",
    "status": "recording deleted"
}


- GET Request: api/social-virtual-event-bbb-recording-settings/{node_id}?_format=json

Get the form for the recording access settings. The settings can be changed any time, so
there is no direct link when creating a meeting. So you can update the recording access
while a meeting is running!

Example Request:

api/social-virtual-event-bbb-recording-settings/f260b040-bee1-4657-ac72-b780ab2b6595?_format=json

Example Response:

{
    "recording_access": {
        "#type": "select",
        "#title": "Who can see the recordings?",
        "#options": {
            "recording_access_viewer": "All visitors (including public)",
            "recording_access_viewer_authenticated": "All authenticated users",
            "recording_access_viewer_moderator": "All Organisers",
            "recording_access_viewer_enrolled": "All Organisers, Group members & enrolled",
            "recording_access_viewer_group": "All Organisers & Group members"
        },
        "#default_value": "recording_access_viewer",
        "#disabled": null
    },
    "join_button_visible_before": {
        "#type": "select",
        "#title": "Display join button before event start",
        "#default_value": "show_always_open",
        "#options": {
            "show_always_open": "Show always open",
            "5": "5 minutes",
            "10": "10 minutes",
            "15": "15 minutes",
            "30": "30 minutes",
            "45": "45 minutes",
            "60": "60 minutes",
            "90": "90 minutes"
        },
        "#disabled": null
    },
    "join_button_visible_after": {
        "#type": "select",
        "#title": "Display join button after event closes",
        "#default_value": "show_always_open",
        "#options": {
            "show_always_open": "Show always open",
            "5": "5 minutes",
            "10": "10 minutes",
            "15": "15 minutes",
            "30": "30 minutes",
            "45": "45 minutes",
            "60": "60 minutes",
            "90": "90 minutes"
        },
        "#disabled": null
    }
}

- POST Request: api/social-virtual-event-bbb-recording-settings/{node_id}?_format=json

Once a user has picked his options, he can submit the following request. Once this has been saved, he
can access when the user meets the criteria returned by the response

Example Request:

api/social-virtual-event-bbb-recording-settings/f260b040-bee1-4657-ac72-b780ab2b6595?_format=json

Example Request body:

{
    "recording_access": "recording_access_viewer",
    "join_button_visible_before": "show_always_open",
    "join_button_visible_after": "show_always_open"
}

Example Response:

{
    "recording_access_settings": "successfully updated"
}






